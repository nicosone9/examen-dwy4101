from django.db.models import Q 
from rest_framework import generics, mixins
from proyecto_compras.models import Persona, Tiendas
from .permissions import IsOwnerOrReadOnly
from .serializers import PersonaSerializer, TiendaSerializer

#Persona

class PersonaAPIView(mixins.CreateModelMixin, generics.ListAPIView):

    lookup_field = 'id'
    serializer_class = PersonaSerializer
    #queryset = Person.objects.all()

    def get_queryset(self):
        qs = Persona.objects.all()
        query = self.request.GET.get("q")
        if query is not None:
            qs = qs.filter(
                    Q(usuario__icontains=query)|
                    Q(email__icontains=query)
                    ).distinct()
        return qs

    def performCrear(self, serializer):
        serializer.save(user=self.request.user)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

class PersonaRUD(generics.RetrieveUpdateDestroyAPIView):

    lookup_field = 'id'
    serializer_class = PersonaSerializer
    permission_classes = [IsOwnerOrReadOnly]
    #queryset = Person.objects.all()

    def get_queryset(self):
        return Persona.objects.all()

#Tiendas

class TiendasAPIView(mixins.CreateModelMixin, generics.ListAPIView):

    lookup_field = 'id'
    serializer_class = TiendaSerializer

    def get_queryset(self):
        qs = Tiendas.objects.all()
        query = self.request.GET.get("q")
        if query is not None:
            qs = qs.filter(
                    Q(nombre__icontains=query)|
                    Q(estado__icontains=query)
                    ).distinct()
        return qs

    def performCrear(self, serializer):
        serializer.save(user=self.request.user)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

class TiendasRUD(generics.RetrieveUpdateDestroyAPIView):

    lookup_field = 'id'
    serializer_class = TiendaSerializer
    permission_classes = [IsOwnerOrReadOnly]

    def get_queryset(self):
        return Tiendas.objects.all()
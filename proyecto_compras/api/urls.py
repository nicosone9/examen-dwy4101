from django.conf.urls import url, include
from .views import PersonaRUD, PersonaAPIView, TiendasRUD, TiendasAPIView
app_name="proyecto_compras"

urlpatterns = [
    url(r'^$', PersonaAPIView.as_view(),name='post-crear'),
    url(r'^(?P<id>\d+)/$', PersonaRUD.as_view(),name='post-rud'),
    url(r'^tiendas/$', TiendasAPIView.as_view(),name='post-crearTienda'),
    url(r'^tiendas/(?P<id>\d+)/$', TiendasRUD.as_view(),name='post-rudTienda'),
]

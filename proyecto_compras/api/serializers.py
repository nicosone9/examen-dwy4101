from rest_framework import serializers
from proyecto_compras.models import Persona, Tiendas

class PersonaSerializer(serializers.ModelSerializer):

    class Meta:
        model = Persona
        fields = '__all__'

    def validarUsuario(self, value):
        qs = Persona.objects.filter(Usuario__iexact=value)
        if self.instance:
            qs = qs.exclude(pk=self.instance.id)
        if qs.exist():
            raise serializers.ValidationError("El usuario debe ser unico")
        return value

class TiendaSerializer(serializers.ModelSerializer):

    class Meta:
        model = Tiendas
        fields = '__all__'
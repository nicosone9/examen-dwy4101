from django import forms
from django.contrib.auth.forms import UserChangeForm
from .models import Tiendas

class ProductoForm(forms.Form):
    nombreProducto = forms.CharField(max_length=100)
    costo_presupuestado = forms.IntegerField()
    costo_real = forms.IntegerField()
    tiendaCompraProducto = forms.CharField(max_length=100)
    notas = forms.CharField(max_length=200) 

class editarTiendaForm(UserChangeForm):
    class Meta:
        model = Tiendas
        fields = (
            'nombre',
            'nombreSucursal',
            'direccion',
            'ciudad',
            'region',
            'aprobado',
        )
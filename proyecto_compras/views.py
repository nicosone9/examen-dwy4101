from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import redirect
from django.views.decorators.http import require_POST
from .models import Persona, Producto, Tiendas
from .forms import editarTiendaForm
from django.db.models import Sum

#importar user
from django.contrib.auth.models import User, Group
#sistema de autenticación 
from django.contrib.auth import authenticate,logout, login as auth_login

from django.contrib.auth.decorators import login_required
from rest_framework import generics
from django.db import models

#forms

from .forms import ProductoForm

# Create your views here.

def index(request):
    pro_lista = Producto.objects.order_by('id')
    form = ProductoForm()
    count = Producto.objects.all().count()
    pre = Producto.objects.all().aggregate(Sum('costo_presupuestado'))
    real = Producto.objects.all().aggregate(Sum('costo_real'))
    tienda = Tiendas.objects.all()
    context = {'pro_lista' : pro_lista, 'form' : form, 'count' : count, 'pre' : pre, 'real' : real, 'tienda' : tienda} 
    return render(request, 'index.html', context)

def admin(request):
    tienda = Tiendas.objects.order_by('id')
    context = {'tienda' : tienda}
    return render(request, 'admin.html', context)

def login(request):
    return render(request,'login.html')

def userLogout(request):
    logout(request)
    return redirect('login')

def registro(request):
    return render(request,'registro.html')

def registroTienda(request):
    return render(request,'registrotienda.html')

def crear(request):
    username = request.POST.get('username','')
    email = request.POST.get('email','')
    password = request.POST.get('password','')

    persona = User.objects.create_user(username, email, password)
    persona.save()

    return redirect('index')

def login_iniciar(request):
    username = request.POST.get('username','')
    password = request.POST.get('password','')
    print(username,password)
    user = authenticate(request, username=username, password=password)

    if user is not None:
        if user.is_superuser:
            auth_login(request, user)
            return redirect('admin')     
        else:
            auth_login(request, user)
            return redirect('index')        
    else:
        return redirect('login')

#Vistas relacionadas con la lista de productos
@require_POST
def agregarProducto(request):

    nombreProducto = request.POST.get('nombreProducto', '')
    costo_presupuestado = request.POST.get('costo_presupuestado', '')
    costo_real = request.POST.get('costo_real', False)
    tiendaCompraProducto = request.POST.get('tiendaCompraProducto', '')

    nuevo_producto = Producto(nombreProducto=nombreProducto, costo_presupuestado=costo_presupuestado, costo_real=costo_real, tiendaCompraProducto=tiendaCompraProducto)
    nuevo_producto.save() 


    return redirect('index')

def productoComprado(request, producto_id):
    producto = Producto.objects.get(pk=producto_id)
    producto.comprado = True
    producto.save()

    return redirect('index')

def eliminarComprados(request):
    Producto.objects.filter(comprado__exact=True).delete()

    return redirect('index')

def eliminarTodos(request):
    Producto.objects.all().delete()

    return redirect('index')

def editarProducto(request):
    id = request.POST.get('id', 0)
    nombreProducto = request.POST.get('nombreProducto', '')
    costo_presupuestado = request.POST.get('costo_presupuestado', '')
    costo_real = request.POST.get('costo_real', False)
    tiendaCompraProducto = request.POST.get('tiendaCompraProducto', '')

    producto = Producto.objects.get(pk = id)

    producto.nombreProducto = nombreProducto
    producto.costo_real = costo_real
    producto.tiendaCompraProducto = tiendaCompraProducto
    producto.save()

    return redirect('index')

def agregarTienda(request):

    nombre = request.POST.get('nombreTienda', '')
    nombreSucursal = request.POST.get('nombreSucursal', '')
    direccion = request.POST.get('direccion', False)
    ciudad = request.POST.get('ciudad', '')
    region = request.POST.get('region', '')

    nuevo_tienda = Tiendas(nombre=nombre, nombreSucursal=nombreSucursal, direccion=direccion, ciudad=ciudad, region=region)
    nuevo_tienda.save() 


    return redirect('index')

def aprobarTienda(request, tienda_id):
    tienda = Tiendas.objects.get(pk = tienda_id)
    tienda.aprobado = True
    tienda.save()
    
    return redirect('admin')

# def crearLista(request):
#     newList = request.POST.get('newList', '')

#     newList = []

#     return redirect('index')

###############################test

# class IteamLista(models.Model):
#     producto = models.OneToOneField(Producto, on_delete=models.SET_NULL, null=True)
#     comprado = models.BooleanField(default=False)

#     def __str__(self):
#         return self.producto.nombreProducto

# class Lista(models.Model):
#     nombre = models.CharField(max_length=15)
#     cuenta = models.ForeignKey(Persona, on_delete=models.SET_NULL, null=True)
#     items = models.ManyToManyField(ItemLista)

#     def totalPre(self):
#         return sum([item.producto.costo_presupuesto for item in self.items.all()])

#     def __str__(self):
#         return '{0} - {1}'.format(self.cuenta, self.nombre)

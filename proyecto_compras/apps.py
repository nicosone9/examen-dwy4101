from django.apps import AppConfig


class ProyectoComprasConfig(AppConfig):
    name = 'proyecto_compras'

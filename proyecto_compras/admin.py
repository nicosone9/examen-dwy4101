from django.contrib import admin

# Register your models here.

from .models import Producto, Tiendas
admin.site.register(Producto)
admin.site.register(Tiendas)

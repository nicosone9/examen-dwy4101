from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from . import views
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic import TemplateView
from django.conf.urls import url, include

urlpatterns = [
    
    path('',views.login,name="login"),
    path('index/',views.index, name="index"),
    path('administrador/', views.admin, name='admin'),
    path('administrador/aprobar/<tienda_id>', views.aprobarTienda, name='aprobarTienda'),
    path('login/login_iniciar',views.login_iniciar,name="login_iniciar"),
    path('index/userLogout', views.userLogout, name="userLogout"),
    path('registro/',views.registro,name='registro'),
    path('registro/crear',views.crear,name='crear'),
    path('agregarProducto/', views.agregarProducto,name='agregarProducto'),
    path('comprado/<producto_id>', views.productoComprado, name='comprado'),
    path('eliminarComprados', views.eliminarComprados, name='eliminarComprados'),
    path('eliminarTodos', views.eliminarTodos, name='eliminarTodos'),
    path('index/editarProducto/', views.editarProducto, name='editarProducto'),
    path('registroTienda/', views.registroTienda, name='registroTienda'),
    path('registroTienda/agregar', views.agregarTienda, name='agregarTienda'),


    # url(r'^manifest.json', (TemplateView.as_view(template_name="manifest.json", content_type='application/json', )), name='manifest.json'),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += format_suffix_patterns(urlpatterns)
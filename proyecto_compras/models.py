from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Persona(models.Model):
    email = models.CharField(max_length=100)
    username = models.CharField(max_length=50)
    password = models.CharField(max_length=20)
    
    def __str__(self):
        return "persona" 


class Producto(models.Model):
    nombreProducto = models.CharField(max_length=100)
    costo_presupuestado = models.BigIntegerField()
    costo_real = models.IntegerField()
    tiendaCompraProducto = models.CharField(max_length=100)
    notas = models.CharField(max_length=200) 
    comprado= models.BooleanField(default=False)

    def __str__(self):
        return "producto" 

class Tiendas(models.Model):
    nombre = models.CharField(max_length=50)
    nombreSucursal = models.CharField(max_length=50)
    direccion = models.CharField(max_length=100)
    ciudad = models.CharField(max_length=100)
    region = models.CharField(max_length=100)
    aprobado = models.BooleanField(default=False)

    def __str__(self):
        return "tiendas" 

